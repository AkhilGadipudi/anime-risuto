# Anime Risuto - [Video Demo](https://www.youtube.com/watch?v=AyZNNe7stg8)

Anime Risuto is an app for people who watch anime and want to keep track of them, maintain a library.
The app provides ways to discover new anime, search for particular anime that they know, add them to
various lists, get pictures and videos of the anime, find anime related to the ones you are interested in,
and keep track of anime they recently checked out. Overall, anime risuto provides all the important
features required to maintain an anime library with a simple and elegant UI

## [Detailed Explanation](https://bitbucket.org/AkhilGadipudi/anime-risuto/src/master/AnimeRisuto_Doc.pdf)