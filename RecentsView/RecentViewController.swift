//
//  RecentViewController.swift
//  Anime Risuto
//
//  Created by MA on 5/1/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit
import Firebase

class RecentViewController: UITableViewController {
    var list: [RecentItem] = []
    
    
    let cellid = "cellid"
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! RecentTableCell
        if(list.count>0){
            let item = list[indexPath.item]
            cell.poster.downloadedFrom(link: item.image_url!)
            cell.title.text = item.title!
            cell.status.text = "Status : " + item.status!
            cell.score.text = "Score : " + String((Double(item.score!)!/10) * 100) + "%"
        }
        cell.setCellShadow()
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsView = DetailsViewController()
        let item = list[indexPath.item]
        let animeCell = CellModel(id:Int(item.mal_id!)!, image:item.image_url!, title:item.title!)
        detailsView.animeCell = animeCell
        detailsView.mainView = self
        navigationController?.pushViewController(detailsView, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = list[indexPath.item]
            list.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .fade)
            FBhelper?.removeFromRecents(mal_id: item.mal_id!)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
        getRecents {
            self.list.sort {
                Int($0.rank)! > Int($1.rank)!
            }
            self.tableView.reloadData()
        }
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.backgroundColor = MyColors.lightBackground
        navigationController?.navigationBar.barTintColor = MyColors.lightBackground
        navigationController?.navigationBar.tintColor = MyColors.tintBlue
        navigationController?.navigationBar.isTranslucent = false
        let textArib = [NSAttributedStringKey.foregroundColor: MyColors.tintBlue]
        navigationController?.navigationBar.titleTextAttributes = textArib
        self.navigationItem.title = "Recents"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = MyColors.darkBackground
        tableView.separatorColor = MyColors.darkBackground
        tableView.register(RecentTableCell.self, forCellReuseIdentifier: cellid)
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
    }
    
    private func getRecents(completed: @escaping () -> () ){
        let ref = FBhelper?.userDb?.child("recents")
        ref?.observeSingleEvent(of: .value, with: {(snapshot) in
            if snapshot.childrenCount > 0 {
                self.list.removeAll()
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    if(item.key == "holder") {continue}
                    if let dictionary = item.value as? [String: String] {
                        let recentItem = RecentItem(dictionary)
                        self.list.append(recentItem)
                    }
                }
                completed()
            }
        })
    }
}


class RecentTableCell: UITableViewCell {
    
    let title: UILabel = {
        let label = UILabel()
        label.text = "Testing a very very very very long Title Here"
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.numberOfLines = 0
        label.padding = UIEdgeInsets(top: 5, left: 0, bottom: 10, right: 0)
        label.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 22.0)
        return label
    }()
    
    let poster: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "image_placeholder.jpg")
        image.contentMode = .scaleToFill
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = UIColor.clear
        return image
    }()
    
    let status: UILabel = {
        let label = UILabel()
        label.text = "Status"
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = MyColors.textColor
        label.numberOfLines = 1
        label.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18.0)
        return label
    }()
    
    let score: UILabel = {
        let label = UILabel()
        label.text = "Score"
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = MyColors.textColor
        label.numberOfLines = 1
        label.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18.0)
        return label
    }()

    private func setupViews(){
        contentView.addSubview(poster)
        contentView.addSubview(title)
        contentView.addSubview(score)
        contentView.addSubview(status)
        contentView.backgroundColor = MyColors.lightBackground
        contentView.tintColor = MyColors.darkBackground
        
        
        poster.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        poster.heightAnchor.constraint(equalToConstant: 100).isActive = true
        poster.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        poster.widthAnchor.constraint(equalToConstant: 80).isActive = true
        poster.setCellShadow()
        
        //title.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        title.leadingAnchor.constraint(equalTo: poster.trailingAnchor, constant: 20).isActive = true
        title.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        title.heightAnchor.constraint(equalToConstant: 50).isActive = true
        title.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        
        score.leadingAnchor.constraint(equalTo: poster.trailingAnchor, constant: 20).isActive = true
        score.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        //score.heightAnchor.constraint(equalToConstant: 40).isActive = true
        score.topAnchor.constraint(equalTo: title.bottomAnchor).isActive = true
        
        status.leadingAnchor.constraint(equalTo: poster.trailingAnchor, constant: 20).isActive = true
        status.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true
        status.topAnchor.constraint(equalTo: score.bottomAnchor, constant: 10).isActive = true
        //status.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
