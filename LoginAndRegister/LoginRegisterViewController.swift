//
//  LoginRegisterViewController.swift
//  Assignment3
//
//  Created by MA on 4/9/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit
import Firebase

class LoginRegisterViewController: UIViewController {
    
    var topWindow: UIWindow!
    
    let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login", for: UIControlState())
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = MyColors.tintBlue
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    
    let registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Register", for: UIControlState())
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = MyColors.lightBackground
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(handleRegister), for: .touchUpInside)
        return button
    }()
    

    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor: MyColors.textColor])
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.cornerRadius = 8
        tf.backgroundColor = MyColors.lightBackground
        tf.textColor = .white
        return tf
    }()
    
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: MyColors.textColor])
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.isSecureTextEntry = true
        tf.layer.cornerRadius = 8
        tf.backgroundColor = MyColors.lightBackground
        tf.textColor = .white
        tf.tintColor = MyColors.textColor
        return tf
    }()
    
    let titleView: UILabel = {
        let label = UILabel()
        label.text = "Anime Risuto"
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 36)
        
        return label
    }()
    
    @objc func reset() {
        let ud = UserDefaults.standard
        print("Reset Executed")
        ud.set(false, forKey: defaultKeys.hasLuanchedKey)
        ud.set(0, forKey: defaultKeys.recentsCountKey)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        setupViews()
        
        let data = UserDefaults.standard
        let email = data.string(forKey: defaultKeys.emailKey)
        let password = data.string(forKey: defaultKeys.passwordKey)
        
        if(email! != "" && password! != ""){
            Login(email: email!, password: password!)
        }
        // Should be removed
        titleView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(reset))
        titleView.addGestureRecognizer(tap)
    }
    
    func setupViews() {
        self.view.backgroundColor = MyColors.darkBackground
        view.addSubview(titleView)
        view.addSubview(loginButton)
        view.addSubview(registerButton)
        view.addSubview(emailTextField)
        view.addSubview(passwordTextField)
        
        titleView.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 100, paddingLeft: 10, paddingBottom: 0, paddingRight: 10)
        emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailTextField.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 20).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        passwordTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 10).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        loginButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 20).isActive = true
        loginButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        registerButton.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 20).isActive = true
        registerButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        registerButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    @objc func handleLogin() {
        self.view.endEditing(true)
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            print("Must enter email and password")
            Messages.Error(self, title: "Input Error", msg: "Must Enter Email and Password")
            return
        }
        Login(email: email,password: password)
    }
    
    private func Login(email: String, password: String){
        // email = "test@gmail.com"
        // password = "password"
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if error != nil {
                print(error ?? "")
                Messages.Error(self,title: "Login Error", msg: "Login Failed\nCheck Credentials And Internet Connection")
                return
            }
            //successfully logged in
            let data = UserDefaults.standard
            data.set(email, forKey: defaultKeys.emailKey)
            data.set(password, forKey: defaultKeys.passwordKey)
            self.fetchUserAndProcede {
                let tabView = TabBarController()
                let tabnav = UINavigationController(rootViewController: tabView)
                self.topWindow.rootViewController = tabnav
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    private func fetchUserAndProcede(completed: @escaping () -> ()) {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = User(dictionary: dictionary)
                FBhelper = FBHelper(user)
                completed()
            }
        }, withCancel: nil)
    }
    
    @objc func handleRegister() {
        self.view.endEditing(true)
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            Messages.Error(self,title: "Input Error", msg: "Must Enter Email and Password")
            return
        }
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if error != nil {
                print(error ?? "")
                Messages.Error(self,title: "Registration Error", msg: "Could Not Create Account")
                return
            }
            guard (user?.uid) != nil else {
                return
            }
            // Do something with new account
            let profileEditView = ProfileEditViewController()
            profileEditView.newAccount = true
            self.navigationController?.pushViewController(profileEditView, animated: true)
            self.dismiss(animated: true, completion: nil)
        })
    }
    
}
