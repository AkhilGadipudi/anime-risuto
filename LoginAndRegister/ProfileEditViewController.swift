//
//  ProfileEditViewController.swift
//  Assignment3
//
//  Created by MA on 4/9/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit
import Firebase

class ProfileEditViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    var newAccount: Bool = false
    var profileurl: String?
    lazy var profileImage: UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "image_placeholder.jpg")
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleToFill
        img.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectProfileImage)))
        img.isUserInteractionEnabled = true
        return img
    }()
    
    let nameTextField: UITextField = {
        let tf = UITextField()
        tf.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedStringKey.foregroundColor: MyColors.textColor])
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.cornerRadius = 8
        tf.backgroundColor = MyColors.lightBackground
        tf.textColor = .white
        return tf
    }()

    let contryTextField: UITextField = {
        let tf = UITextField()
        tf.attributedPlaceholder = NSAttributedString(string: "Contry", attributes: [NSAttributedStringKey.foregroundColor: MyColors.textColor])
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.cornerRadius = 8
        tf.backgroundColor = MyColors.lightBackground
        tf.textColor = .white
        return tf
    }()
    
    let dobField: UITextField = {
        let tf = UITextField()
        tf.attributedPlaceholder = NSAttributedString(string: "Date of Birth", attributes: [NSAttributedStringKey.foregroundColor: MyColors.textColor])
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.cornerRadius = 8
        tf.backgroundColor = MyColors.lightBackground
        tf.textColor = .white
        return tf
    }()
    
    
    let submitButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Submit", for: UIControlState())
        button.layer.cornerRadius = 10
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = MyColors.tintBlue
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(newAccount) {
            self.setupViews()
        }
        else {
            fetchUserProfileAndFillData {
                self.setupViews()
            }
        }
    }
    
    func setupViews() {
        view.addSubview(profileImage)
        view.addSubview(nameTextField)
        view.addSubview(contryTextField)
        view.addSubview(dobField)
        view.addSubview(submitButton)
        view.backgroundColor = MyColors.darkBackground
        
        profileImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        profileImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        profileImage.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        profileImage.heightAnchor.constraint(equalTo: profileImage.widthAnchor).isActive = true
        
        nameTextField.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 20).isActive = true
        nameTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        nameTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        
        contryTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: 10).isActive = true
        contryTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        contryTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        
        dobField.topAnchor.constraint(equalTo: contryTextField.bottomAnchor, constant: 10).isActive = true
        dobField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dobField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        
        submitButton.topAnchor.constraint(equalTo: dobField.bottomAnchor, constant: 10).isActive = true
        submitButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        submitButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3).isActive = true
    }
    
    
    @objc func selectProfileImage() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }

    // Get image to upload using image picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        if let selectedImage = selectedImageFromPicker {
            profileImage.image = selectedImage
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("canceled image picker")
        dismiss(animated: true, completion: nil)
    }
    
    // Submit button action for adding user data to firebase
    @objc func handleSubmit() {
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        let imageName = UUID().uuidString
        let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
        if let profileImage = self.profileImage.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
            
            _ = storageRef.putData(uploadData, metadata: nil) { (metadata, error) in
                guard let metadata = metadata else {
                    print("Error when uploading profile image")
                    return
                }
                self.profileurl = metadata.downloadURL()?.absoluteString
                self.registerUserIntoFirebaseWithUID(uid)
            }
        }
    }
    
    // Puts entered user data in Firebase
    fileprivate func registerUserIntoFirebaseWithUID(_ uid: String) {
        let ref = Database.database().reference()
        let usersReference = ref.child("users").child(uid)
        
        let values = ["id":uid, "name": nameTextField.text!,"contry": contryTextField.text!,"dob": dobField.text! , "profileurl": profileurl] as! [String : String]
        
        usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            if err != nil {
                print(err ?? "")
                return
            }
            usersReference.child("recents").child("holder").setValue("nil")
            self.navigationController?.popViewController(animated: true)
            Messages.Error(self, title: "Done", msg: "Account Created")
        })
    }
    
    // In case of editing existing profile, fills fourm with current data
    func fetchUserProfileAndFillData(completed: @escaping () -> ()) {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = User(dictionary: dictionary)
                self.setupProfileWithUser(user)
                completed()
            }
        }, withCancel: nil)
    }
    

    
    func setupProfileWithUser(_ user: User) {
        if let url = user.profileurl {
            profileImage.downloadedFrom(link: url)
        }
        if let name = user.name {
            nameTextField.text = name
        }
        if let contry = user.contry {
            contryTextField.text = contry
        }
        if let dob = user.dob {
            dobField.text = dob
        }
    }
}
