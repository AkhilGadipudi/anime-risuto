//
//  SeasonsCollectionView.swift
//  Anime Risuto
//
//  Created by MA on 4/24/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class SeasonCollectionView: UICollectionViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    var mainCollectionVIew: UICollectionViewController?
    let cellid = "SeasonCellId"
    let seasonsList = ["winter 2018","fall 2018", "summer 2018", "spring 2018", "winter 2017", "fall 2017", "summer 2018", "spring 2017"]
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let seasonsListTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Anime By Season"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        return label
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return seasonsList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! SeasonCell
        cell.title = seasonsList[indexPath.item]
        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height))
        overlay.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.2)
        cell.seasonPoster.addSubview(overlay)
        cell.setCellShadow()
        cell.seasonPoster.setCellShadow()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("season cell tapped")
        let seasonString = self.seasonsList[indexPath.item]
        downloadSeasonList(seasonString: seasonString) {
            
        }
    }
    
    private func downloadSeasonList(seasonString: String, completed: @escaping () -> () ){
        let split = seasonString.components(separatedBy: [" "]).filter { $0 != "" }
        let season = split[0].trimmingCharacters(in: .whitespaces)
        let year = split[1]
        let urlString = "https://api.jikan.moe/season/\(year)/\(season)"
        guard let url = URL(string: urlString) else {
            print("Error creating URL")
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            print("download season List called")
            if(err == nil){
                guard let data = data else {
                    print("No Json Data")
                    return
                }
                //                let dataAsString = String(data: data, encoding: .utf8)
                //                print(dataAsString)
                do  {
                    let myList = try JSONDecoder().decode(SeasonListModel.self, from: data)
                    print("season Json parsing sucess")
                    DispatchQueue.main.async {
                        let layout = UICollectionViewFlowLayout()
                        let viewAllView = ViewAllCollectionView(collectionViewLayout: layout)
                        viewAllView.list = myList.list
                        viewAllView.mainCollectionView = self.mainCollectionVIew
                        self.mainCollectionVIew?.navigationController?.pushViewController(viewAllView, animated: true)
                        completed()
                    }
                } catch let jsonErr {
                    print("Erron parsing Json:", jsonErr)
                }
            }
            }.resume()
    }
    
    private func setupViews() {
        backgroundColor = .clear
        addSubview(collectionView)
        addSubview(seasonsListTitle)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SeasonCell.self, forCellWithReuseIdentifier: cellid)
        seasonsListTitle.setAnchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, paddingTop: 20, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width:  0.80 * frame.width, height: 40)
        collectionView.setAnchor(top: seasonsListTitle.bottomAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }
}

class SeasonCell: UICollectionViewCell {
    var year: String?
    var poster: String? {
        didSet{
            seasonPoster.image = UIImage(named: poster!)
            seasonPoster.setCellShadow()
            
        }
    }
    var title: String? {
        didSet{
            let split = title?.components(separatedBy: [" "]).filter { $0 != "" }
            poster = split?[0].trimmingCharacters(in: .whitespaces)
            year = split?[1]
            seasonTitle.text = title?.capitalized
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let seasonPoster: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.layer.cornerRadius = 10
        iv.layer.masksToBounds = true
        iv.clipsToBounds = true
        iv.contentMode = .scaleToFill
        iv.image  = #imageLiteral(resourceName: "image_placeholder")
        return iv
    }()
    
    let seasonTitle: UILabel = {
        let title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.font = UIFont.boldSystemFont(ofSize: 26)
        title.textColor = .white
        title.numberOfLines = 1
        title.textAlignment = .center
        title.text = "Season Title"
        return title
    }()
 
    private func setupViews() {
       
        addSubview(seasonPoster)
        addSubview(seasonTitle)
        contentView.setCellShadow()
        seasonPoster.setCellShadow()
        seasonPoster.setAnchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        seasonTitle.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        seasonTitle.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
    }
}
