//
//  ListCollectionView.swift
//  Anime Risuto
//
//  Created by MA on 4/21/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class ListCollectionViewCell: UICollectionViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    private let cellid = "ListCollectionCellId"
    var mainCollectionView: UICollectionViewController?
    var list: [CellModel] = []
    var apiUrl: String? {
        didSet{
            downloadList {
                self.setupViews()
                self.animeCollectionView.reloadData()
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let animeCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! AnimeCell
        cell.anime = list[indexPath.item]
        cell.setCellShadow()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        zoomAnim(cv: collectionView, indexPath: indexPath, anime: list[indexPath.item])
    }
    
    private func zoomAnim(cv: UICollectionView, indexPath: IndexPath, anime: CellModel){
        let iv = UIImageView()
        let attrib: UICollectionViewLayoutAttributes  = cv.layoutAttributesForItem(at: indexPath)!
        attrib.frame = cv.convert(attrib.frame, to: self.mainCollectionView?.view)
        attrib.frame.size = CGSize(width: attrib.frame.width, height: 0.7 * attrib.frame.height)
        iv.frame = attrib.frame
        iv.downloadedFrom(link: anime.image_url)
        iv.setCellShadow()
        let screen = UIScreen.main.bounds
        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: screen.width, height: screen.height))
        overlay.backgroundColor = UIColor.black
        overlay.alpha = 0
        self.mainCollectionView?.view.addSubview(overlay)
        self.mainCollectionView?.view.addSubview(iv)
        let detailsView = DetailsViewController()
        detailsView.animeCell = anime
        detailsView.mainView = self.mainCollectionView
        let toframe = CGRect(x: 15, y: 25, width: 150, height: 220)
        
        UIView.animate(withDuration: 1, animations: {
            iv.frame = toframe
            overlay.alpha = 1
        }, completion: { _ in
            self.mainCollectionView?.view.willRemoveSubview(iv)
            iv.removeFromSuperview()
            self.mainCollectionView?.view.willRemoveSubview(overlay)
            overlay.removeFromSuperview()
            self.mainCollectionView?.navigationController?.pushViewController(detailsView, animated: false)
        })
    }
    
    let listTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "List Title"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        return label
    }()
    
    let listViewAllButton: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "View All"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = MyColors.tintBlue
        label.isUserInteractionEnabled = true
        return label
    }()
    
    @objc func viewAllClicked() {
        print("view all clicked")
        let layout = UICollectionViewFlowLayout()
        let viewAllView = ViewAllCollectionView(collectionViewLayout: layout)
        viewAllView.list = self.list
        viewAllView.mainCollectionView = mainCollectionView
        mainCollectionView?.navigationController?.pushViewController(viewAllView, animated: true)
    }
    
    private func setupViews() {
        backgroundColor = UIColor.clear
        addSubview(animeCollectionView)
        addSubview(listTitle)
        addSubview(listViewAllButton)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewAllClicked))
        listViewAllButton.addGestureRecognizer(gesture)
        animeCollectionView.dataSource = self
        animeCollectionView.delegate = self
        animeCollectionView.register(AnimeCell.self, forCellWithReuseIdentifier: cellid)
        animeCollectionView.setAnchor(top: listTitle.bottomAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        listTitle.setAnchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, paddingTop: 20, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width:  0, height: 40)
        listViewAllButton.setAnchor(top: contentView.topAnchor, left: nil, bottom: nil, right: contentView.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight:15, width: 0, height: 40)
    }
    
    
    
    // Download list from url
    private func downloadList(completed: @escaping () -> () ){
        //let urlString = "https://api.jikan.moe/top/anime/1/airing"
        let urlString = apiUrl
        guard let url = URL(string: urlString!) else {
            print("Error creating URL")
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            print("download List called")
            if(err == nil){
                guard let data = data else {
                    print("No Json Data")
                    return
                }
                //                let dataAsString = String(data: data, encoding: .utf8)
                //                print(dataAsString)
                do  {
                    let myList = try JSONDecoder().decode(TopListModel.self, from: data)
                    self.list = myList.list
                    print("Json parsing sucess")
                    DispatchQueue.main.async {
                        completed()
                    }
                } catch let jsonErr {
                    print("Erron parsing Json:", jsonErr)
                }
            }
            }.resume()
    } // downloadList end
}
