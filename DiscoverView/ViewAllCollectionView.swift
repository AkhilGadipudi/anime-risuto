//
//  ViewAllCollectionView.swift
//  Anime Risuto
//
//  Created by MA on 4/25/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class ViewAllCollectionView: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var list: [CellModel] = []
    private let cellid = "cellid"
    var mainCollectionView: UIViewController?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = MyColors.darkBackground
        collectionView?.register(AnimeCell.self, forCellWithReuseIdentifier: cellid)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! AnimeCell
        if list.count > 0 { cell.anime = list[indexPath.item] }
        cell.setCellShadow()
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        zoomAnim(cv: collectionView, indexPath: indexPath, anime: list[indexPath.item])
    }
    
    private func zoomAnim(cv: UICollectionView, indexPath: IndexPath, anime: CellModel){
        let iv = UIImageView()
        let attrib: UICollectionViewLayoutAttributes  = cv.layoutAttributesForItem(at: indexPath)!
        attrib.frame = cv.convert(attrib.frame, to: self.view)
        attrib.frame.size = CGSize(width: attrib.frame.width, height: 0.7 * attrib.frame.height)
        iv.frame = attrib.frame
        iv.downloadedFrom(link: anime.image_url)
        iv.setCellShadow()
        let screen = UIScreen.main.bounds
        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: screen.width, height: screen.height))
        overlay.backgroundColor = UIColor.black
        overlay.alpha = 0
        self.view.addSubview(overlay)
        self.view.addSubview(iv)
        let detailsView = DetailsViewController()
        detailsView.animeCell = anime
        detailsView.mainView = self
        let toframe = CGRect(x: 15, y: 25, width: 150, height: 220)
        
        UIView.animate(withDuration: 1, animations: {
            iv.frame = toframe
            overlay.alpha = 1
        }, completion: { _ in
            self.view.willRemoveSubview(iv)
            iv.removeFromSuperview()
            self.view.willRemoveSubview(overlay)
            overlay.removeFromSuperview()
            self.navigationController?.pushViewController(detailsView, animated: false)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 115, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}
