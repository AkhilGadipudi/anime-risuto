//
//  CollectionViewController.swift
//  Anime Risuto
//
//  Created by MA on 4/21/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class CollectionVIewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    private let cellid = "CollectionViewCellId"
    private let seasoncellid = "SeasonCollectionView"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.backgroundColor = MyColors.lightBackground
        navigationController?.navigationBar.barTintColor = MyColors.lightBackground
        navigationController?.navigationBar.tintColor = MyColors.tintBlue
        navigationController?.navigationBar.isTranslucent = false
        let textArib = [NSAttributedStringKey.foregroundColor: MyColors.tintBlue]
        navigationController?.navigationBar.titleTextAttributes = textArib
        self.navigationItem.title = "Discover"
        navigationController?.navigationBar.isHidden = false
        let logoutButton = UIBarButtonItem(title: "Logout", style: .plain , target: self, action: #selector(Logout))
        self.navigationItem.leftBarButtonItem = logoutButton
    }
    
    @objc func Logout(){
        let userDefaults = UserDefaults.standard
        userDefaults.set("",forKey: defaultKeys.emailKey)
        userDefaults.set("", forKey: defaultKeys.passwordKey)
        let login = LoginRegisterViewController()
        login.topWindow = globalWindow
        let loginNav = UINavigationController(rootViewController: login)
        globalWindow?.rootViewController = loginNav
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = MyColors.darkBackground
        collectionView?.register(ListCollectionViewCell.self, forCellWithReuseIdentifier: cellid)
        collectionView?.register(SeasonCollectionView.self, forCellWithReuseIdentifier: seasoncellid)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    let urls = [ "https://api.jikan.moe/top/anime/1/airing", "https://api.jikan.moe/top/anime/1/bypopularity", "https://api.jikan.moe/top/anime/1/upcoming"]
    let titles = ["Top Airing", "Most Popular", "Top Upcoming"]
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: seasoncellid, for: indexPath) as! SeasonCollectionView
            cell.mainCollectionVIew = self
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! ListCollectionViewCell
        cell.mainCollectionView = self
        if indexPath.item > 1 {
            cell.apiUrl = urls[indexPath.item - 1]
            cell.listTitle.text = titles[indexPath.item - 1]
            return cell
        }
        cell.apiUrl = urls[indexPath.item]
        cell.listTitle.text = titles[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 1 {
            return CGSize(width: view.frame.width, height: 180)
        }
        return CGSize(width: view.frame.width, height: 320)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left:0, bottom: 10, right: 0)
    }
}
