//
//  MediaCollectionView.swift
//  Anime Risuto
//
//  Created by MA on 4/30/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class MediaCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    var type: String = " "
    var image_urls: [String] = []
    var videos: [Video] = []
    let imageCellId = "imagecell"
    let videosCellId = "videocell"
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.dataSource = self
        self.delegate = self
        register(ImageCell.self, forCellWithReuseIdentifier: imageCellId)
        register(VideoCell.self, forCellWithReuseIdentifier: videosCellId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(type == "images") { return image_urls.count}
        else if(type == "videos") {return videos.count}
        else {return 0}
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(type == "images") { return CGSize(width:150, height: 220)}
        else if(type == "videos") {return CGSize(width: 300, height: 200)}
        else {return CGSize.zero}
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(type == "images"){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageCellId, for: indexPath) as! ImageCell
            cell.image_url = image_urls[indexPath.item]
            cell.setCellShadow()
            return cell
        } else if (type == "videos") {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: videosCellId, for: indexPath) as! VideoCell
            cell.video = videos[indexPath.item]
            cell.setCellShadow()
            return cell
        } else {return UICollectionViewCell()}
    }
    
    @objc func handleTap(){
        let views = self.superview?.subviews
        for sview in views! {
            if sview.tag == 1000 {
                sview.removeFromSuperview()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(type == "images"){
            let iv = UIImageView()
            let attrib: UICollectionViewLayoutAttributes  = collectionView.layoutAttributesForItem(at: indexPath)!
            attrib.frame.size = CGSize(width: attrib.frame.width, height: attrib.frame.height)
            attrib.frame = collectionView.convert(attrib.frame, to: collectionView.superview)
            iv.frame = attrib.frame
            iv.downloadedFrom(link: image_urls[indexPath.item])
            iv.setCellShadow()
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            
            let screen = UIScreen.main.bounds
            let overlay = UIView(frame: CGRect(x: 0, y: 0, width: screen.width, height: screen.height))
            overlay.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.4)
            overlay.isUserInteractionEnabled = true
            overlay.addGestureRecognizer(tap)
            overlay.addSubview(iv)
            overlay.tag = 1000
            self.superview?.addSubview(overlay)
            let toframe = CGRect(x: (screen.width/2) - 125, y: (screen.height/2) - 210, width: 250, height: 340)
            UIView.animate(withDuration: 1, animations: {
                iv.frame = toframe
            }, completion: { _ in })
        }
    }
}

class ImageCell: UICollectionViewCell {
    var image_url: String? {
        didSet{
            imageView.downloadedFrom(link: image_url!)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.layer.masksToBounds = true
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .clear
        iv.image = #imageLiteral(resourceName: "image_placeholder")
        return iv
    }()
    
    private func setupViews() {
        backgroundColor = MyColors.lightBackground
        addSubview(imageView)
        imageView.frame = CGRect(x:0, y:0, width: frame.width, height: frame.height)
        imageView.setCellShadow()
    }
}

class VideoCell: UICollectionViewCell {
    var video: Video? {
        didSet{
            let url = URL(string: (video?.video_url)!)
            webView.loadRequest(URLRequest(url: url!))
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let webView: UIWebView = {
        let wv = UIWebView()
        wv.translatesAutoresizingMaskIntoConstraints = false
        return wv
    }()
    
    private func setupViews() {
        backgroundColor = MyColors.lightBackground
        addSubview(webView)
        webView.frame = CGRect(x:0, y:0, width: frame.width, height: frame.height)
        webView.setCellShadow()
    }
}
