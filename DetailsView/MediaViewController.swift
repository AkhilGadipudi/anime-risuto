//
//  MediaViewController.swift
//  Anime Risuto
//
//  Created by MA on 4/30/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class MediaViewController: UIViewController {
    var mainView: UIViewController?
    var image_urls: [String] = []
    var videos: [Video] = []
    var anime: DetailsModel?
    var animeCell: CellModel? {
        didSet{
            self.downloadImages {
                if(self.image_urls.count == 0) {
                    self.imagesEmptyView.isHidden = false
                    self.imageCollection.isHidden = true
                } else {
                    print("got images")
                    self.imagesEmptyView.isHidden = true
                    self.imageCollection.isHidden = false
                    self.imageCollection.image_urls = self.image_urls
                    self.imageCollection.reloadData()
                }
            }
            self.downloadVideos {
                if(self.videos.count == 0) {
                    self.videosEmptyView.isHidden = false
                    self.videosCollection.isHidden = true
                } else {
                    print("got videos")
                    self.videosEmptyView.isHidden =  true
                    self.videosCollection.isHidden = false
                    self.videosCollection.videos = self.videos
                    self.videosCollection.reloadData()
                }
            }
        }
    }
    
    let segmentView = UISegmentedControl()
    private func setupSegementView() {
        segmentView.frame = CGRect(x: 0, y: 0, width: 250, height: 30)
        segmentView.backgroundColor =  MyColors.lightBackground
        segmentView.tintColor = MyColors.tintBlue
        segmentView.layer.cornerRadius = 8
        segmentView.removeAllSegments()
        segmentView.insertSegment(withTitle: "Info", at: 0, animated: false)
        segmentView.insertSegment(withTitle: "Media", at: 1, animated: false)
        segmentView.insertSegment(withTitle: "Related", at: 2, animated: false)
        segmentView.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        segmentView.selectedSegmentIndex = 1
    }
    
    @objc private func selectionDidChange(_ sender: UISegmentedControl) {
        if(sender.selectedSegmentIndex == 0) { // Change to Info
            let detailsView = DetailsViewController()
            detailsView.setAnime = true
            detailsView.anime = self.anime
            detailsView.calledBySegment = true
            detailsView.mainView = self.mainView
            mainView?.navigationController?.popViewController(animated: false)
            mainView?.navigationController?.pushViewController(detailsView, animated: false)
        } else if(sender.selectedSegmentIndex == 2) { //Change to Related list
            let relatedDic = anime?.related
            var related:[RelatedItem] = []
            for array in Array(relatedDic!.values) {
                related = related + array
            }
            let layout = UICollectionViewFlowLayout()
            let relatedView = RelatedViewController(collectionViewLayout: layout)
            relatedView.relatedlist = related
            relatedView.animeCell = self.animeCell
            relatedView.mainView = self.mainView
            mainView?.navigationController?.popViewController(animated: false)
            mainView?.navigationController?.pushViewController(relatedView, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.backgroundColor = MyColors.lightBackground
        navigationController?.navigationBar.barTintColor = MyColors.lightBackground
        navigationController?.navigationBar.tintColor = MyColors.tintBlue
        navigationController?.navigationBar.isTranslucent = false
        setupSegementView()
        self.navigationItem.titleView = segmentView
    }
    
    let imageCollection: MediaCollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let v = MediaCollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        v.type = "images"
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .clear
        return v
    }()
    let videosCollection: MediaCollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let v = MediaCollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        v.type = "videos"
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .clear
        return v
    }()
    
    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false;
        view.isDirectionalLockEnabled = true
        view.alwaysBounceHorizontal = false
        return view
    }()
    let stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false;
        view.axis = .vertical
        view.distribution = .fill
        return view
    }()
    
    let imagesHeadingView: UILabel = {
        let label = UILabel()
        label.text = "Pictures"
        label.numberOfLines = 1
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.padding = UIEdgeInsets(top: 20, left: 15, bottom: 10, right: 0)
        label.font = UIFont.boldSystemFont(ofSize: 24)
        return label
    }()
    
    let videosHeadingView: UILabel = {
        let label = UILabel()
        label.text = "Videos"
        label.numberOfLines = 1
        label.textColor = UIColor.white
        label.padding = UIEdgeInsets(top: 20, left: 15, bottom: 10, right: 0)
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 24)
        return label
    }()
    
    let imagesEmptyView: UILabel = {
        let label = UILabel()
        label.text = "No Pictures Available"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.padding = UIEdgeInsets(top: 20, left: 15, bottom: 10, right: 0)
        label.font = UIFont.italicSystemFont(ofSize: 20)
        return label
    }()
    
    let videosEmptyView: UILabel = {
        let label = UILabel()
        label.text = "No Videos Available"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.padding = UIEdgeInsets(top: 20, left: 15, bottom: 10, right: 0)
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.font = UIFont.italicSystemFont(ofSize: 20)
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setConstraints()
    }
    
    private func setupViews() {
        view.backgroundColor = MyColors.darkBackground
        stackView.addArrangedSubview(imagesHeadingView)
        stackView.addArrangedSubview(imagesEmptyView)
        stackView.addArrangedSubview(imageCollection)
        stackView.addArrangedSubview(videosHeadingView)
        stackView.addArrangedSubview(videosEmptyView)
        stackView.addArrangedSubview(videosCollection)
        scrollView.addSubview(stackView)
        view.addSubview(scrollView)
    }
    
    private func setConstraints(){
        scrollView.setAnchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        scrollView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.98).isActive = true
        scrollView.heightAnchor.constraint(equalToConstant: self.view.frame.height).isActive = true
        
        stackView.setAnchor(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: scrollView.bottomAnchor, right: scrollView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        
        imageCollection.setAnchor(top: nil, left: stackView.leftAnchor, bottom: nil, right: stackView.rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 10)
        imageCollection.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
        imageCollection.heightAnchor.constraint(equalToConstant: 230).isActive = true
        
        videosCollection.setAnchor(top: nil, left: stackView.leftAnchor, bottom: nil, right: stackView.rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 10)
        videosCollection.heightAnchor.constraint(equalToConstant: 210).isActive = true
        videosCollection.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
        
    }
    
    
    func downloadImages(completed: @escaping () -> () ){
        let id = "\(animeCell?.mal_id ?? 1)"
        print("id inside donwload images : ", id)
        let urlString = "https://api.jikan.moe/anime/\(id)/pictures"
        guard let url = URL(string: urlString) else {
            print("Error creating URL")
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            print("download List called")
            if(err == nil){
                guard let data = data else {
                    print("No Json Data")
                    return
                }
                do  {
                    let details = try JSONDecoder().decode(DetailsModel.self, from: data)
                    self.anime = details
                    let images: DetailsImages = try JSONDecoder().decode(DetailsImages.self, from: data)
                    self.image_urls = images.image
                    DispatchQueue.main.async {
                        completed()
                    }
                } catch let jsonErr { print("Erron parsing Details Json:", jsonErr)  }
            }
            }.resume()
    }
    func downloadVideos(completed: @escaping () -> () ){
        let id = "\(animeCell?.mal_id ?? 1)"
        print("id inside donwload videos : ", id)
        let urlString = "https://api.jikan.moe/anime/\(id)/videos"
        guard let url = URL(string: urlString) else {
            print("Error creating URL")
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            print("download List called")
            if(err == nil){
                guard let data = data else {
                    print("No Json Data")
                    return
                }
                do  {
                    let videos: DetailsVideos = try JSONDecoder().decode(DetailsVideos.self, from: data)
                    self.videos = videos.promo
                    DispatchQueue.main.async {
                        completed()
                    }
                } catch let jsonErr {
                    print("Erron parsing Details Json:", jsonErr)
                    DispatchQueue.main.async {
                        print("completed with no videos")
                        completed()
                    }
                }
            }
            }.resume()
    }
}
