//
//  DetailViews.swift
//  Anime Risuto
//
//  Created by MA on 4/29/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit
import QuartzCore

// Primary View - has Title, poster, score, status and button for adding to list
class PrimaryInfoView: UIView {
    
    let posterView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .clear
        //iv.image = #imageLiteral(resourceName: "image_placeholder")
        iv.setCellShadow()
        return iv
    }()
    
    let titleView: UILabel = {
        let label = UILabel()
        label.text = "Anime Title Goes Here"
        label.numberOfLines = 2
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 24)
        return label
    }()
    
    
    let scoreStringView: UILabel = {
        let label = UILabel()
        label.text = "0.0 / 10"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let statusView: UILabel = {
        let label = UILabel()
        label.text = "Status"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    let listButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("  List Name  ", for: UIControlState())
        button.backgroundColor = MyColors.tintBlue
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        return button
    }()
    
//    let favButton: UIButton = {
//        let button = UIButton()
//        button.translatesAutoresizingMaskIntoConstraints = false
//        button.setImage(#imageLiteral(resourceName: "fav-deselect"), for: .normal)
//        button.tintColor = MyColors.tintRed
//        return button
//    }()
    
    let line: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false;
        v.backgroundColor = MyColors.lightBackground
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(posterView)
        addSubview(titleView)
        //addSubview(scoreView)
        addSubview(scoreStringView)
        addSubview(statusView)
        addSubview(listButton)
        //addSubview(favButton)
        addSubview(line)
        
        posterView.setAnchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: nil, paddingTop: 15, paddingLeft: 15, paddingBottom: 10, paddingRight: 0, width: 150, height: 220)
        titleView.setAnchor(top: self.topAnchor, left: posterView.rightAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 15, paddingLeft: 15, paddingBottom: 0, paddingRight: 5,width:0, height: 60)
        scoreStringView.setAnchor(top: titleView.bottomAnchor, left: posterView.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 15, paddingBottom: 0, paddingRight: 0, width: 0, height: 40)
        statusView.setAnchor(top: scoreStringView.bottomAnchor, left: posterView.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 15, paddingBottom: 0, paddingRight: 0,width: 0,height: 40)
        listButton.setAnchor(top: nil, left: posterView.rightAnchor, bottom: posterView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 15, paddingBottom: 0, paddingRight: 0)
        listButton.widthAnchor.constraint(lessThanOrEqualToConstant: 200).isActive = true
        listButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        //favButton.setAnchor(top: self.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, paddingTop: 15, paddingLeft: 0, paddingBottom: 0, paddingRight: 5, width: 50, height: 50)
        line.setAnchor(top: nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        line.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        line.heightAnchor.constraint(equalToConstant: 3).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// Extra Info View - Contains aditional info on the anime
class ExtraInfoView: UIView {
    let infoHeadingView: UILabel = {
        let label = UILabel()
        label.text = "Details"
        label.numberOfLines = 1
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let typeView: UILabel = {
        let label = UILabel()
        label.text = "Type"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.padding = UIEdgeInsets(top:0,left:10,bottom:0,right:0)
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    let airedView: UILabel = {
        let label = UILabel()
        label.text = "Aired From To"
        label.numberOfLines = 1
        label.padding = UIEdgeInsets(top:0,left:10,bottom:0,right:0)
        label.textColor = MyColors.textColor
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    let sourceView: UILabel = {
        let label = UILabel()
        label.text = "Source"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.padding = UIEdgeInsets(top:0,left:10,bottom:0,right:0)
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    let episodesView: UILabel = {
        let label = UILabel()
        label.text = "Episodes Count"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.padding = UIEdgeInsets(top:0,left:10,bottom:0,right:0)
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    let ratingView: UILabel = {
        let label = UILabel()
        label.text = "Rating"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.padding = UIEdgeInsets(top:0,left:10,bottom:0,right:0)
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    let stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false;
        view.axis = .vertical
        view.distribution = .fill
        return view
    }()
    
    let line: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false;
        v.backgroundColor = MyColors.lightBackground
        return v
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        stackView.addArrangedSubview(infoHeadingView)
        stackView.addArrangedSubview(typeView)
        stackView.addArrangedSubview(sourceView)
        stackView.addArrangedSubview(episodesView)
        stackView.addArrangedSubview(airedView)
        stackView.addArrangedSubview(ratingView)
        addSubview(stackView)
        addSubview(line)
        stackView.setAnchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0)
        stackView.widthAnchor.constraint(equalTo:self.widthAnchor).isActive = true
        line.setAnchor(top: stackView.bottomAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        line.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        line.heightAnchor.constraint(equalToConstant: 3).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// Genres View - Collection view of the genres of the anime
class GenresView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    var genres = ["Genre 1", "Gnere 2", "Genre 3"] {
        didSet{
            genreCollectionView.reloadData()
        }
    }
    let genresHeadingView: UILabel = {
        let label = UILabel()
        label.text = "Genres"
        label.numberOfLines = 1
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let line: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false;
        v.backgroundColor = MyColors.lightBackground
        return v
    }()
    
    let genreCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return genres.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! GenreCell
        cell.setCellShadow()
        cell.genreLabel.text = genres[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let tempLabel: UILabel = {
            let label = UILabel()
            label.text = genres[indexPath.item]
            label.numberOfLines = 1
            label.textColor = MyColors.textColor
            label.textAlignment = .center
            label.backgroundColor = .clear
            label.font = UIFont.boldSystemFont(ofSize: 16)
            return label
        }()
        let size = tempLabel.intrinsicContentSize
        return CGSize(width: size.width + 20, height: 40)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(genresHeadingView)
        addSubview(genreCollectionView)
        addSubview(line)
        genreCollectionView.dataSource = self
        genreCollectionView.delegate = self
        genreCollectionView.register(GenreCell.self, forCellWithReuseIdentifier: "cellid")
        
        genresHeadingView.setAnchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0)
        genreCollectionView.setAnchor(top: genresHeadingView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 10)
        genreCollectionView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        genreCollectionView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        line.setAnchor(top: genreCollectionView.bottomAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        line.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        line.heightAnchor.constraint(equalToConstant: 3).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// Cell for genres collection view
class GenreCell: UICollectionViewCell {
    let genreLabel: UILabel = {
        let label = UILabel()
        label.text = "Genre Name"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = MyColors.lightBackground
        genreLabel.setCellShadow()
        addSubview(genreLabel)
        genreLabel.setAnchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        contentView.widthAnchor.constraint(equalToConstant: 100).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DescriptionView: UIView {
    let descriptionHeadingView: UILabel = {
        let label = UILabel()
        label.text = "Synopsis"
        label.numberOfLines = 1
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Synopis Text"
        label.numberOfLines = 0
        label.textColor = MyColors.textColor
        label.padding = UIEdgeInsets(top:0,left:10,bottom:0,right:0)
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    let line: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false;
        v.backgroundColor = MyColors.lightBackground
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(descriptionHeadingView)
        addSubview(descriptionLabel)
        addSubview(line)
        descriptionHeadingView.setAnchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0)
        descriptionLabel.setAnchor(top: descriptionHeadingView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 0)
        line.setAnchor(top: descriptionLabel.bottomAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        line.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        line.heightAnchor.constraint(equalToConstant: 3).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

