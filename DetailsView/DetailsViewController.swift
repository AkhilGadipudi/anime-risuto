//
//  DetailsViewController.swift
//  Anime Risuto
//
//  Created by MA on 4/27/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    var mainView: UIViewController?
    var currentList: String = "nil"
    var sharedFrame: CGRect = CGRect.zero
    
    var animeCell: CellModel? {
        didSet{
            if(setAnime){return}
            downloadDetails { (error) in
                if(error){
                    Messages.Error(self,title: "Download Error", msg: "Error Downloading Details")
                    return
                }
                print("details obtained")
                self.setValues()
                if(!self.calledBySegment){
                    self.addToRecents()
                }
            }
        }
    }
    var setAnime: Bool = false
    var calledBySegment: Bool = false
    var anime: DetailsModel? {
        didSet{
            if(setAnime){
                self.setValues()
                let Cell = CellModel(id: (anime?.mal_id)!, image: (anime?.image_url)!, title: (anime?.title)!)
                self.animeCell = Cell
            }
            self.getCurrentList()
        }
    }
    
    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false;
        view.isDirectionalLockEnabled = true
        view.alwaysBounceHorizontal = false
        return view
    }()
    
    let stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false;
        view.axis = .vertical
        view.distribution = .fill
        return view
    }()
    
    private func addToRecents() {
            let id = anime?.mal_id
            let score = anime?.score
            let values = ["mal_id": String("\(id ?? 0)"), "title": (anime?.title)!, "image_url": (anime?.image_url)!,"score": String("\(score ?? 0)"), "status": (anime?.status)!]
            FBhelper?.addToRecents(values: values)
    }
    
    let segmentView = UISegmentedControl()
    private func setupSegementView() {
        segmentView.frame = CGRect(x: 0, y: 0, width: 250, height: 30)
        segmentView.backgroundColor =  MyColors.lightBackground
        segmentView.tintColor = MyColors.tintBlue
        segmentView.layer.cornerRadius = 8
        segmentView.removeAllSegments()
        segmentView.insertSegment(withTitle: "Info", at: 0, animated: false)
        segmentView.insertSegment(withTitle: "Media", at: 1, animated: false)
        segmentView.insertSegment(withTitle: "Related", at: 2, animated: false)
        segmentView.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        segmentView.selectedSegmentIndex = 0
    }
    
    
    
    @objc private func selectionDidChange(_ sender: UISegmentedControl) {
        if(sender.selectedSegmentIndex == 1) { //Change to Media
            let mediaView = MediaViewController()
            mediaView.mainView = self.mainView
            mediaView.animeCell = self.animeCell
            mainView?.navigationController?.popViewController(animated: false)
            mainView?.navigationController?.pushViewController(mediaView, animated: false)
        } else if(sender.selectedSegmentIndex == 2) { // Change to Related
            let relatedDic = anime?.related
            var related:[RelatedItem] = []
            for array in Array(relatedDic!.values) {
                related = related + array
            }
            let layout = UICollectionViewFlowLayout()
            let relatedView = RelatedViewController(collectionViewLayout: layout)
            relatedView.relatedlist = related
            relatedView.animeCell = self.animeCell
            relatedView.mainView = self.mainView
            mainView?.navigationController?.popViewController(animated: false)
            mainView?.navigationController?.pushViewController(relatedView, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.backgroundColor = MyColors.lightBackground
        navigationController?.navigationBar.barTintColor = MyColors.lightBackground
        navigationController?.navigationBar.tintColor = MyColors.tintBlue
        navigationController?.navigationBar.isTranslucent = false
        setupSegementView()
        self.navigationItem.titleView = segmentView
        
       
    }
    
    private func getCurrentList() {
        let idString = String("\(anime?.mal_id ?? 1)")
        if(FBhelper?.Lists.w2w.contains(idString))! {
            self.currentList = ListKeys.w2w
            self.primaryInfo.listButton.setTitle("  Want To Watch  ", for: UIControlState())
        }
        else if(FBhelper?.Lists.watching.contains(idString))! {
            self.currentList = ListKeys.watching
            self.primaryInfo.listButton.setTitle("  Watching  ", for: UIControlState())
        }
        else if(FBhelper?.Lists.completed.contains(idString))! {
            self.currentList = ListKeys.completed
            self.primaryInfo.listButton.setTitle("  Completed  ", for: UIControlState())
        }
        else if(FBhelper?.Lists.dropped.contains(idString))! {
            self.currentList = ListKeys.dropped
            self.primaryInfo.listButton.setTitle("  Dropped  ", for: UIControlState())
        }
        else {
            self.currentList = "nil"
            self.primaryInfo.listButton.setTitle("  Add To Library  ", for: UIControlState())
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = MyColors.darkBackground
        populateStack()
        scrollView.addSubview(stackView)
        view.addSubview(scrollView)
        setConstraints()
        if(sharedFrame != CGRect.zero){
            let toframe = CGRect(x: 15, y: 25, width: 150, height: 220)
            let iv = UIImageView()
            iv.downloadedFrom(link: (animeCell?.image_url)!)
            iv.setCellShadow()
            iv.frame = sharedFrame
            let screen = UIScreen.main.bounds
            let overlay = UIView(frame: CGRect(x: 0, y: 0, width: screen.width, height: screen.height))
            overlay.backgroundColor = UIColor.black
            view.addSubview(overlay)
            view.addSubview(iv)
            UIView.animate(withDuration: 1, animations: {
                iv.frame = toframe
                overlay.alpha = 0
            }, completion: { _ in
                self.view.willRemoveSubview(iv)
                self.view.willRemoveSubview(overlay)
                overlay.removeFromSuperview()
                iv.removeFromSuperview()
            })
        }
    }
    let primaryInfo = PrimaryInfoView()
    let extraInfo = ExtraInfoView()
    let genres = GenresView()
    let descriptionView = DescriptionView()
    
    private func populateStack() {
        primaryInfo.listButton.addTarget(self, action: #selector(handleListButton), for: .touchUpInside)
        stackView.addArrangedSubview(primaryInfo)
        stackView.addArrangedSubview(genres)
        stackView.addArrangedSubview(extraInfo)
        stackView.addArrangedSubview(descriptionView)
    }
    
    private func setValues() {
        primaryInfo.posterView.downloadedFrom(link: (anime?.image_url)!)
        primaryInfo.titleView.text = anime?.title
        primaryInfo.scoreStringView.text = "Score : " + String(((anime?.score)!/10) * 100) + "%"
        primaryInfo.statusView.text = "Status : " + (anime?.status)!
        
        extraInfo.airedView.text = "Air Period : " + (anime?.aired_string)!
        var episodes  =  "\(anime?.episodes ?? 0)"
        if(anime?.episodes == 0) {episodes = "TBD"}
        extraInfo.episodesView.text = "Episodes : " + episodes
        extraInfo.ratingView.text = "Rating : " + (anime?.rating)!
        extraInfo.sourceView.text = "Source : " + (anime?.source)!
        extraInfo.typeView.text = "Type : " + (anime?.type)!
        
        genres.genres = GenreToString((anime?.genre)!)
        descriptionView.descriptionLabel.text = anime?.synopsis
        
        
    }
    
    private func setConstraints() {
        
        scrollView.setAnchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        scrollView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.98).isActive = true
        //scrollView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 1).isActive = true
        scrollView.heightAnchor.constraint(equalToConstant: self.view.frame.height - 50).isActive = true
        
        stackView.setAnchor(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: scrollView.bottomAnchor, right: scrollView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        
        primaryInfo.setAnchor(top: nil, left: stackView.leftAnchor, bottom: nil, right: stackView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        primaryInfo.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
        primaryInfo.heightAnchor.constraint(equalToConstant: 245).isActive = true
        
        genres.setAnchor(top: nil, left: stackView.leftAnchor, bottom: nil, right: stackView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        genres.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
        genres.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        extraInfo.setAnchor(top: nil, left: stackView.leftAnchor, bottom: nil, right: stackView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        extraInfo.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
        
        descriptionView.setAnchor(top: nil, left: stackView.leftAnchor, bottom: nil, right: stackView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        descriptionView.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
    }
    
    
    ////////
    func downloadDetails (completed: @escaping ((Bool) -> Void) ){
        let id = "\(animeCell?.mal_id ?? 1)"
        var error = false
        print("id inside donwload details : ", id)
        let urlString = "https://api.jikan.moe/anime/\(id)/pictures"
        guard let url = URL(string: urlString) else {
            print("Error creating URL")
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            if(err == nil){
                guard let data = data else {
                    print("No Json Data")
                    return
                }
                do  {
                    print(data)
                    let details: DetailsModel = try JSONDecoder().decode(DetailsModel.self, from: data)
                    self.anime = details
                     error = false
                    DispatchQueue.main.async {
                        completed(error)
                    }
                } catch let jsonErr {
                    print("Erron parsing Details Json:", jsonErr)
                    error = true
                    completed(error)
                }
            }
        }.resume()
    }
    
    @objc private func handleListButton() {
        let alertController = UIAlertController(title: "Update Libray", message: "Select List", preferredStyle: .alert)
        
        // Actions
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        let w2w = UIAlertAction(title: "Want To Watch",style: .default) { (_) in
            self.primaryInfo.listButton.setTitle("  Want To Watch  ", for: UIControlState())
            FBhelper?.updateList(from: self.currentList, to: ListKeys.w2w , id: (self.anime?.mal_id)!)
            self.currentList = ListKeys.w2w
        }
        let watching = UIAlertAction(title: "Watching", style: .default) { (_) in
            self.primaryInfo.listButton.setTitle("  Watching  ", for: UIControlState())
            FBhelper?.updateList(from: self.currentList, to: ListKeys.watching , id: (self.anime?.mal_id)!)
            self.currentList = ListKeys.watching
        }
        let completed = UIAlertAction(title: "Completed", style: .default) {(_) in
            self.primaryInfo.listButton.setTitle("  Completed  ", for: UIControlState())
            FBhelper?.updateList(from: self.currentList, to: ListKeys.completed , id: (self.anime?.mal_id)!)
            self.currentList = ListKeys.completed
        }
        let dropped = UIAlertAction(title: "Dropped", style: .default) { (_) in
            self.primaryInfo.listButton.setTitle("  Dropped  ", for: UIControlState())
            FBhelper?.updateList(from: self.currentList, to: ListKeys.dropped , id: (self.anime?.mal_id)!)
            self.currentList = ListKeys.dropped
        }
        let remove = UIAlertAction(title: "Remove From Library", style: .destructive) { (_) in
            self.primaryInfo.listButton.setTitle("  Add To Library  ", for: UIControlState())
            FBhelper?.updateList(from: self.currentList, to: "nil" , id: (self.anime?.mal_id)!)
            self.currentList = "nil"
        }
        
        alertController.addAction(w2w)
        alertController.addAction(watching)
        alertController.addAction(completed)
        alertController.addAction(dropped)
        if(self.currentList != "nil") { alertController.addAction(remove)}
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
