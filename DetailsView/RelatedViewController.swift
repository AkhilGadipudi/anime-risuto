//
//  ViewAllCollectionView.swift
//  Anime Risuto
//
//  Created by MA on 4/25/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class RelatedViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var mainView: UIViewController?
    var animeCell: CellModel? {
        didSet{
            //print("Related animeCell : ",animeCell!)
        }
    }
    var relatedlist: [RelatedItem]? {
        didSet{
            downloadDetails(items: relatedlist!) {
                print("Related Details Obteined")
                self.collectionView?.reloadData()
            }
        }
    }
    var list: [DetailsModel] = []
    private let cellid = "cellid"
    
    let segmentView = UISegmentedControl()
    private func setupSegementView() {
        segmentView.frame = CGRect(x: 0, y: 0, width: 250, height: 30)
        segmentView.backgroundColor =  MyColors.lightBackground
        segmentView.tintColor = MyColors.tintBlue
        segmentView.layer.cornerRadius = 8
        segmentView.removeAllSegments()
        segmentView.insertSegment(withTitle: "Info", at: 0, animated: false)
        segmentView.insertSegment(withTitle: "Media", at: 1, animated: false)
        segmentView.insertSegment(withTitle: "Related", at: 2, animated: false)
        segmentView.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        segmentView.selectedSegmentIndex = 2
    }
    
    @objc private func selectionDidChange(_ sender: UISegmentedControl) {
        if(sender.selectedSegmentIndex == 0) { // Change to Info
            let detailsView = DetailsViewController()
            detailsView.setAnime = false
            detailsView.calledBySegment = true
            detailsView.animeCell = self.animeCell
            detailsView.mainView = self.mainView
            mainView?.navigationController?.popViewController(animated: false)
            mainView?.navigationController?.pushViewController(detailsView, animated: false)
        } else if(sender.selectedSegmentIndex == 1) { //Change to Media
            let mediaView = MediaViewController()
            mediaView.mainView = self.mainView
            mediaView.animeCell = self.animeCell
            mainView?.navigationController?.popViewController(animated: false)
            mainView?.navigationController?.pushViewController(mediaView, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.backgroundColor = MyColors.lightBackground
        navigationController?.navigationBar.barTintColor = MyColors.lightBackground
        navigationController?.navigationBar.tintColor = MyColors.tintBlue
        navigationController?.navigationBar.isTranslucent = false
        setupSegementView()
        self.navigationItem.titleView = segmentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = MyColors.darkBackground
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.register(AnimeCell.self, forCellWithReuseIdentifier: cellid)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! AnimeCell
        if(list.count > 0) {
            let anime = list[indexPath.item]
            cell.cellPoster.downloadedFrom(link: anime.image_url)
            cell.cellTitle.text = anime.title
            cell.setCellShadow()
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsView = DetailsViewController()
        detailsView.setAnime = true
        detailsView.anime = list[indexPath.item]
        detailsView.mainView = mainView
        mainView?.navigationController?.popViewController(animated: false)
        mainView?.navigationController?.pushViewController(detailsView, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 115, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    
    func downloadDetails (items: [RelatedItem], completed: @escaping () -> () ){
        var count = items.count
        self.list.removeAll()
        for item in items {
            if(item.type == "manga") {
                count = count - 1
                continue
            }
            let id = item.mal_id
            let urlString = "https://api.jikan.moe/anime/\(id)/"
            guard let url = URL(string: urlString) else {
                print("Error creating URL")
                return
            }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                if(err == nil){
                    guard let data = data else {
                        print("No Json Data")
                        return
                    }
                    do  {
                        let details: DetailsModel = try JSONDecoder().decode(DetailsModel.self, from: data)
                        self.list.append(details)
                        count = count - 1
                        if(count == 0) {
                            DispatchQueue.main.async {
                                completed()
                            }
                        }
                    } catch let jsonErr {
                        print("Erron parsing Json:", jsonErr)
                    }
                }
                }.resume()
        }
    }
}
