//
//  SegmentViewController.swift
//  Anime Risuto
//
//  Created by MA on 4/30/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class SegmentViewController : UIViewController {
   let segmentView = UISegmentedControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        setupSegementView()
    }
    
    private func setupSegementView() {
        segmentView.removeAllSegments()
        segmentView.insertSegment(withTitle: "Info", at: 0, animated: false)
        segmentView.insertSegment(withTitle: "Media", at: 1, animated: false)
        segmentView.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
    }
    
    @objc private func selectionDidChange(_ sender: UISegmentedControl) {
        
    }
}
