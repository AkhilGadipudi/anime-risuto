//
//  LibraryViewController.swift
//  Anime Risuto
//
//  Created by MA on 5/3/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class LibraryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var idList: [String] = []
    var animeList: [DetailsModel] = []
    let cellid = "cellid"
    var didLoad = false
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return animeList.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 115, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! AnimeCell
        if animeList.count > 0 {
            let anime = animeList[indexPath.item]
            let animeCell = CellModel(id: anime.mal_id, image: anime.image_url, title: anime.title)
            cell.anime = animeCell
        }
        cell.setCellShadow()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !(indexPath.item > animeList.count){
            zoomAnim(cv: collectionView, indexPath: indexPath, anime: animeList[indexPath.item])
        }
    }
    
    private func zoomAnim(cv: UICollectionView, indexPath: IndexPath, anime: DetailsModel){
        let iv = UIImageView()
        let attrib: UICollectionViewLayoutAttributes  = cv.layoutAttributesForItem(at: indexPath)!
        attrib.frame = cv.convert(attrib.frame, to: cv.superview)
        attrib.frame.size = CGSize(width: attrib.frame.width, height: 0.7 * attrib.frame.height)
        iv.frame = attrib.frame
        iv.downloadedFrom(link: anime.image_url)
        iv.setCellShadow()
        let screen = UIScreen.main.bounds
        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: screen.width, height: screen.height))
        overlay.backgroundColor = UIColor.black
        overlay.alpha = 0
        self.view.addSubview(overlay)
        self.view.addSubview(iv)
        let detailsView = DetailsViewController()
        detailsView.setAnime = true
        detailsView.anime = anime
        detailsView.mainView = self
        let toframe = CGRect(x: 15, y: 25, width: 150, height: 220)
        
        UIView.animate(withDuration: 1, animations: {
            iv.frame = toframe
            overlay.alpha = 1
        }, completion: { _ in
            self.view.willRemoveSubview(iv)
            iv.removeFromSuperview()
            self.view.willRemoveSubview(overlay)
            overlay.removeFromSuperview()
            self.navigationController?.pushViewController(detailsView, animated: false)
        })
    }
    
    
    private func setupNavBar() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.backgroundColor = MyColors.lightBackground
        navigationController?.navigationBar.barTintColor = MyColors.lightBackground
        navigationController?.navigationBar.tintColor = MyColors.tintBlue
        navigationController?.navigationBar.isTranslucent = false
        let textArib = [NSAttributedStringKey.foregroundColor: MyColors.tintBlue]
        navigationController?.navigationBar.titleTextAttributes = textArib
        self.navigationItem.title = "Library"
    }
    
    let collection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let v = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .clear
        return v
    }()
    
    let listEmptyView: UILabel = {
        let label = UILabel()
        label.text = "No Anime In List"
        label.numberOfLines = 1
        label.textColor = MyColors.textColor
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.padding = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        label.font = UIFont.italicSystemFont(ofSize: 20)
        return label
    }()
    
    let listSelectLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Select List : "
        l.textColor = .white
        l.font = UIFont.boldSystemFont(ofSize: 18)
        return l
    }()
    
    let listSelectButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.setTitle("  List Name  ", for: UIControlState())
        button.tintColor = MyColors.tintBlue
        button.layer.cornerRadius = 8
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.backgroundColor = MyColors.lightBackground
        button.addTarget(self, action: #selector(handleSelectList), for: .touchUpInside)
        return button
    }()
    
    let sortSelectLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Sort By : "
        l.textColor = .white
        l.font = UIFont.boldSystemFont(ofSize: 18)
        return l
    }()
    
    let sortSelectButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.setTitle("  Score  ", for: UIControlState())
        button.tintColor = MyColors.tintBlue
        button.layer.cornerRadius = 8
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.backgroundColor = MyColors.lightBackground
        button.addTarget(self, action: #selector(handleSelectSort), for: .touchUpInside)
        return button
    }()
    
    let listMenu = DropDown()
    let sortMenu = DropDown()
    private func setupMenus(){

        listMenu.anchorView = listSelectButton
        listMenu.animationduration = 0.3
        listMenu.textFont = UIFont.boldSystemFont(ofSize: 16)
        listMenu.textColor = .white
        listMenu.selectionBackgroundColor = MyColors.darkBackground
        listMenu.backgroundColor = MyColors.lightBackground
        listMenu.selectRow(at: 0)
        listMenu.separatorColor = MyColors.lightBackground
        listMenu.dataSource = ["Watching", "Want To Watch", "Completed", "Dropped"]
        listMenu.selectionAction = { [unowned self] (index: Int, item: String) in
            if(item == "Watching"){
                self.getDetailsAndReload((FBhelper?.Lists.watching)!)
                self.listSelectButton.setTitle("  Watching  ", for: UIControlState())
            } else if(item == "Want To Watch"){
                self.getDetailsAndReload((FBhelper?.Lists.w2w)!)
                self.listSelectButton.setTitle("  Want To Watch  ", for: UIControlState())
            } else if(item == "Completed"){
                self.getDetailsAndReload((FBhelper?.Lists.completed)!)
                self.listSelectButton.setTitle("  Completed  ", for: UIControlState())
            } else if(item == "Dropped"){
                self.getDetailsAndReload((FBhelper?.Lists.dropped)!)
                self.listSelectButton.setTitle("  Dropped  ", for: UIControlState())
            }
            self.listMenu.hide()
        }
        
        sortMenu.anchorView = sortSelectButton
        sortMenu.animationduration = 0.3
        sortMenu.textFont = UIFont.boldSystemFont(ofSize: 16)
        sortMenu.textColor = .white
        sortMenu.selectRow(at: 0)
        sortMenu.separatorColor = MyColors.lightBackground
        sortMenu.backgroundColor = MyColors.lightBackground
        sortMenu.selectionBackgroundColor = MyColors.darkBackground
        sortMenu.dataSource = ["Score","Popularity","Favorites"]
        sortMenu.selectionAction = { [unowned self] (index: Int, item: String) in
            self.sortList(item)
            self.sortSelectButton.setTitle(item, for: UIControlState())
            self.sortMenu.hide()
        }
    }
    
    @objc private func handleSelectList(){
       listMenu.show()
    }
    @objc private func handleSelectSort() {
        sortMenu.show()
    }
    
    
    private func getDetailsAndReload(_ idList: [String]){
        self.idList = idList
        if(self.idList.count == 0){
            self.listEmptyView.isHidden = false
            self.collection.isHidden = true
            self.animeList.removeAll()
            self.collection.reloadData()
            return
        }
        downloadDetails(idList: self.idList){
            self.listEmptyView.isHidden = true
            self.collection.isHidden = false
            self.sortList(self.sortSelectButton.title(for: UIControlState())!)
        }
    }
    
    private func sortList(_ by: String){
        switch by {
        case "Score":
            animeList.sort { Double($0.score) > Double($1.score) }
        case "Popularity":
            animeList.sort { $0.popularity > $1.popularity }
        case "Favorites":
            animeList.sort { $0.favorites > $1.favorites }
        default:
            print("Invalid Sort Option")
        }
        self.collection.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
        if(didLoad){
            guard let item = self.listMenu.selectedItem else {
                return
            }
            if(item == "Watching"){
                self.getDetailsAndReload((FBhelper?.Lists.watching)!)
            } else if(item == "Want To Watch"){
                self.getDetailsAndReload((FBhelper?.Lists.w2w)!)
            } else if(item == "Completed"){
                self.getDetailsAndReload((FBhelper?.Lists.completed)!)
            } else if(item == "Dropped"){
                self.getDetailsAndReload((FBhelper?.Lists.dropped)!)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        didLoad = true
        setupViews()
        setConstraints()
        setupMenus()
        self.listSelectButton.setTitle("  Watching  ", for: UIControlState())
        getDetailsAndReload((FBhelper?.Lists.watching)!)
        self.sortSelectButton.setTitle("Score", for: UIControlState())
        sortList("Score")
    }
    
    private func setupViews(){
        collection.dataSource = self
        collection.delegate = self
        collection.register(AnimeCell.self, forCellWithReuseIdentifier: cellid)
        view.backgroundColor = MyColors.darkBackground
        view.addSubview(collection)
        view.addSubview(listSelectButton)
        view.addSubview(listSelectLabel)
        view.addSubview(sortSelectButton)
        view.addSubview(sortSelectLabel)
        view.addSubview(listEmptyView)
    }
    private func setConstraints() {
        listSelectLabel.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 15, paddingLeft: 15, paddingBottom: 0, paddingRight: 0, width: 100, height: 40)
        listSelectButton.setAnchor(top: view.topAnchor, left: listSelectLabel.rightAnchor, bottom: nil, right: nil, paddingTop: 15, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 160, height: 40)
        sortSelectLabel.setAnchor(top: listSelectLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 15, paddingBottom: 0, paddingRight: 0, width: 100, height: 40)
        sortSelectButton.setAnchor(top: listSelectButton.bottomAnchor, left: sortSelectLabel.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 160, height: 40)
        listEmptyView.setAnchor(top: sortSelectLabel.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        collection.setAnchor(top: sortSelectLabel.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }
    
    
    func downloadDetails (idList: [String], completed: @escaping () -> () ){
        var count = idList.count
        self.animeList.removeAll()
        for id in idList {
            let urlString = "https://api.jikan.moe/anime/\(id)/"
            guard let url = URL(string: urlString) else {
                print("Error creating URL")
                return
            }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                if(err == nil){
                    guard let data = data else {
                        print("No Json Data")
                        return
                    }
                    do  {
                        let details: DetailsModel = try JSONDecoder().decode(DetailsModel.self, from: data)
                        self.animeList.append(details)
                        count = count - 1
                        if(count == 0) {
                            DispatchQueue.main.async {
                                completed()
                            }
                        }
                    } catch let jsonErr {
                        print("Erron parsing Json:", jsonErr)
                    }
                }
                }.resume()
        }
    }
}
