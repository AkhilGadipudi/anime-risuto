//
//  SearchViews.swift
//  Anime Risuto
//
//  Created by MA on 5/4/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class SearchBar: UIView {
    let searchTextField: UITextField = {
        let tf = UITextField()
        let paraStyle = NSMutableParagraphStyle()
        paraStyle.alignment = .center
        let atrib = [NSAttributedStringKey.foregroundColor: MyColors.textColor, NSAttributedStringKey.paragraphStyle: paraStyle]
        tf.attributedPlaceholder = NSAttributedString(string: "Search Term", attributes: atrib)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = MyColors.darkBackground
        tf.textColor = .white
        tf.layer.cornerRadius = 8
        return tf
    }()
    let searchButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "search-7"), for: UIControlState())
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = MyColors.lightBackground
        button.tintColor = MyColors.tintBlue
        button.layer.cornerRadius = 8
        //button.addTarget(self, action: #selector(handleSearch), for: .touchUpInside)
        return button
    }()
    
    let filterButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("FILTERS", for: UIControlState())
        button.backgroundColor = MyColors.lightBackground
        button.clipsToBounds = true
        button.setTitleColor(MyColors.tintBlue, for: UIControlState())
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = MyColors.lightBackground
        addSubview(searchTextField)
        addSubview(searchButton)
        addSubview(filterButton)
        setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setConstraints(){
        searchTextField.setAnchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: searchButton.leftAnchor, paddingTop: 5, paddingLeft: 15, paddingBottom: 5, paddingRight: 10, width: 0, height: 40)
        searchButton.setAnchor(top: self.topAnchor, left: nil, bottom: self.bottomAnchor, right: filterButton.leftAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 5, paddingRight: 10, width: 40, height: 40)
        filterButton.setAnchor(top: self.topAnchor, left: nil, bottom: self.bottomAnchor, right: self.rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 5, paddingRight: 15)
        filterButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
}

class FiltersView: UIView {
    var statusString: String = ""
    var scoreString: String = ""
    var genreString: String = ""
    var filterString: String?
    
    let statusToggle: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "circle-tick-7"), for: UIControlState())
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = MyColors.lightBackground
        button.tag = 0
        button.tintColor = MyColors.textColor
        button.addTarget(self, action: #selector(handleToggle), for: .touchUpInside)
        return button
    }()
    let genreToggle: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "circle-tick-7"), for: UIControlState())
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = MyColors.lightBackground
        button.tintColor = MyColors.textColor
        button.tag = 0
        button.addTarget(self, action: #selector(handleToggle), for: .touchUpInside)
        return button
    }()
    let scoreToggle: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "circle-tick-7"), for: UIControlState())
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = MyColors.lightBackground
        button.tintColor = MyColors.textColor
        button.tag = 0
        button.addTarget(self, action: #selector(handleToggle), for: .touchUpInside)
        return button
    }()
    @objc private func handleToggle(_ sender: UIButton){
        if(sender.tag == 0){
            sender.tag = 1
            sender.tintColor = MyColors.tintBlue
        } else if(sender.tag == 1){
            sender.tag = 0
            sender.tintColor = MyColors.textColor
        }
    }
    
    let statusLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Status : "
        l.textColor = .white
        l.font = UIFont.boldSystemFont(ofSize: 16)
        return l
    }()
    let genreLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Type : "
        l.textColor = .white
        l.font = UIFont.boldSystemFont(ofSize: 16)
        return l
    }()
    let scoreLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Score : "
        l.textColor = .white
        l.font = UIFont.boldSystemFont(ofSize: 16)
        return l
    }()
    let statusSelectButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.setTitle("Any", for: UIControlState())
        button.tintColor = MyColors.tintBlue
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.backgroundColor = MyColors.darkBackground
        button.addTarget(self, action: #selector(handleStatusSelect), for: .touchUpInside)
        return button
    }()
    @objc private func handleStatusSelect(){ statusMenu.show() }
    let genreSelectButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.setTitle("Any", for: UIControlState())
        button.tintColor = MyColors.tintBlue
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.backgroundColor = MyColors.darkBackground
        button.addTarget(self, action: #selector(handleGenreSelect), for: .touchUpInside)
        return button
    }()
    @objc private func handleGenreSelect() { genreMenu.show() }
    let statusMenu = DropDown()
    let genreMenu = DropDown()
    private func setupMenus(){
        statusMenu.animationduration = 0.3
        statusMenu.textFont = UIFont.boldSystemFont(ofSize: 16)
        statusMenu.textColor = .white
        statusMenu.selectionBackgroundColor = MyColors.lightBackground
        statusMenu.backgroundColor = MyColors.darkBackground
        statusMenu.anchorView = self.statusSelectButton
        statusMenu.dataSource = ["Any", "Completed", "Airing", "Upcoming"]
        statusMenu.selectionAction = { [unowned self] (index: Int, item: String) in
            if(index == 0){
                self.statusString = ""
            } else if(index == 1){
                self.statusString = "status=completed"
            } else if(index == 2){
                self.statusString = "status=airing"
            } else if(index == 3){
                self.statusString = "status=upcoming"
            }
            self.statusSelectButton.setTitle(item, for: UIControlState())
            self.statusMenu.hide()
        }
        
        genreMenu.animationduration = 0.3
        genreMenu.textFont = UIFont.boldSystemFont(ofSize: 16)
        genreMenu.textColor = .white
        genreMenu.selectionBackgroundColor = MyColors.lightBackground
        genreMenu.backgroundColor = MyColors.darkBackground
        genreMenu.anchorView = self.genreSelectButton
        genreMenu.dataSource = ["Any", "TV", "Movie", "OVA", "Special"]
        genreMenu.selectionAction = { [unowned self] (index: Int, item: String) in
            if(index == 0){
                self.genreString = ""
            } else if(index == 1){
                self.genreString = "type=tv"
            } else if(index == 2){
                self.genreString = "type=movie"
            } else if(index == 3){
                self.genreString = "type=ova"
            }else if(index == 4){
                self.genreString = "type=special"
            }
            self.genreSelectButton.setTitle(item, for: UIControlState())
            self.genreMenu.hide()
        }
    }
    
    let scoreTextField: UITextField = {
        let tf = UITextField()
        tf.keyboardType = .decimalPad
        let paraStyle = NSMutableParagraphStyle()
        paraStyle.alignment = .center
        let atrib = [NSAttributedStringKey.foregroundColor: MyColors.textColor, NSAttributedStringKey.paragraphStyle: paraStyle]
        tf.attributedPlaceholder = NSAttributedString(string: "0.0 - 10.0", attributes: atrib)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = MyColors.darkBackground
        tf.textColor = .white
        tf.layer.cornerRadius = 8
        return tf
    }()
    
    func getFilterString() -> String {
        scoreString = self.scoreTextField.text ?? ""
        var filterString = ""
        if(self.statusToggle.tag == 1 && statusString != ""){
            filterString = filterString + statusString
        }
        if(self.scoreToggle.tag == 1 && scoreString != ""){
            if(filterString != ""){
                filterString = filterString + "&"
            }
            guard let scoreDouble = Double(scoreString) else {
                return "Invalid Score"
            }
            if(scoreDouble > 10.0 || scoreDouble < 0.0){
                return "Score Out Of Range"
            }
            filterString = filterString + "score=" + scoreString
        }
        if(self.genreToggle.tag == 1 && genreString != ""){
            if(filterString != ""){
                filterString = filterString + "&"
            }
            filterString = filterString + genreString
        }
        return filterString
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = MyColors.lightBackground
        addSubview(scoreToggle)
        addSubview(scoreLabel)
        addSubview(scoreTextField)
        addSubview(statusToggle)
        addSubview(statusLabel)
        addSubview(statusSelectButton)
        addSubview(genreToggle)
        addSubview(genreLabel)
        addSubview(genreSelectButton)
        setConstraints()
        setupMenus()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setConstraints(){
        scoreToggle.setAnchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0,width: 40, height: 40)
        scoreLabel.setAnchor(top: self.topAnchor, left: scoreToggle.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0,width: 80, height: 40)
        scoreTextField.setAnchor(top: self.topAnchor, left: scoreLabel.rightAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 10)
        scoreTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        statusToggle.setAnchor(top: scoreToggle.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0,width: 40, height: 40)
        statusLabel.setAnchor(top: scoreToggle.bottomAnchor, left: statusToggle.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0,width: 80, height: 40)
        statusSelectButton.setAnchor(top: scoreToggle.bottomAnchor, left: statusLabel.rightAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 10)
        statusSelectButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        genreToggle.setAnchor(top: statusToggle.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0,width: 40, height: 40)
        genreLabel.setAnchor(top: statusToggle.bottomAnchor, left: genreToggle.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0,width: 80, height: 40)
        genreSelectButton.setAnchor(top: statusToggle.bottomAnchor, left: genreLabel.rightAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 10)
        genreSelectButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
}
