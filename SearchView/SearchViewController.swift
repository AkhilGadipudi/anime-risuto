//
//  SearchViewController.swift
//  Anime Risuto
//
//  Created by MA on 5/4/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    let searchBar = SearchBar()
    let filtersView = FiltersView()
    var filterHidden = true
    var list: [CellModel] = []
    let cellid = "cellid"
    
    
    let resultCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let v = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .clear
        return v
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
        filterHidden = true
        filtersView.frame = CGRect(x: 0, y: -110, width: self.view.frame.width, height: 160)
        resultCollection.frame = CGRect(x: 0, y: 50, width: self.view.frame.width, height: self.view.frame.height - 50)
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.backgroundColor = MyColors.lightBackground
        navigationController?.navigationBar.barTintColor = MyColors.lightBackground
        navigationController?.navigationBar.tintColor = MyColors.tintBlue
        navigationController?.navigationBar.isTranslucent = false
        let textArib = [NSAttributedStringKey.foregroundColor: MyColors.tintBlue]
        navigationController?.navigationBar.titleTextAttributes = textArib
        navigationController?.navigationBar.clipsToBounds = false
       self.navigationItem.title = "Search"
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animateAlongsideTransition(in: nil, animation: nil, completion: { _ in
            self.filterHidden = true
            self.filtersView.frame = CGRect(x: 0, y: -110, width: self.view.frame.width, height: 160)
            self.resultCollection.frame = CGRect(x: 0, y: 50, width: self.view.frame.width, height: self.view.frame.height - 50)
        })
    }
    
    
    override func viewDidLoad() {
        view.backgroundColor = MyColors.darkBackground
        setupViews()
        setConstraints()
    }
    
    private func setupViews() {
        view.addSubview(searchBar)
        searchBar.searchButton.addTarget(self, action: #selector(handleSearchButton), for: .touchUpInside)
        searchBar.filterButton.addTarget(self, action: #selector(handleFiltersButton), for: .touchUpInside)
        view.addSubview(filtersView)
        view.addSubview(resultCollection)
        resultCollection.register(AnimeCell.self, forCellWithReuseIdentifier: cellid)
        resultCollection.dataSource = self
        resultCollection.delegate = self
        view.bringSubview(toFront: searchBar)
    }
    
    private func setConstraints() {
        searchBar.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        searchBar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        filtersView.frame = CGRect(x: 0, y: -110, width: self.view.frame.width, height: 160)
        resultCollection.frame = CGRect(x: 0, y: 210, width: view.frame.width, height: view.frame.height - 210)
    }
    
    @objc private func handleSearchButton() {
        self.view.endEditing(true)
        var urlString = "https://api.jikan.moe/search/anime/"
        let searchTerm = self.searchBar.searchTextField.text ?? ""
        if (searchTerm != "" && searchTerm.count > 2){
            urlString = urlString + searchTerm + "/1"
            if(filterHidden == false){
                let filterString = self.filtersView.getFilterString()
                if(filterString == "Invalid Score" || filterString == "Score Out Of Range"){
                    Messages.Error(self,title: "Input Error", msg: filterString)
                    return
                }
                if(filterString != ""){
                    urlString = urlString + "?" + filterString
                }
            }
            self.downloadList(urlString: urlString){
                print("search count " ,self.list.count)
                self.resultCollection.reloadData()
            }
        } else {
            Messages.Error(self,title: "Input Error", msg: "Search Term Too Short\nMust Be Atlest 3 Characters Long")
        }
    }
    
    private func downloadList(urlString: String, completed: @escaping () -> () ){
        guard let url = URL(string: urlString) else {
            print("Error creating URL")
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            if(err == nil){
                guard let data = data else {
                    print("No Json Data")
                    return
                }
                do  {
                    let myList = try JSONDecoder().decode(SearchListModel.self, from: data)
                    self.list = myList.list
                    print("Search Json parsing sucess")
                    DispatchQueue.main.async {
                        completed()
                    }
                } catch let jsonErr {
                    print("Erron parsing Json:", jsonErr)
                }
            }
            }.resume()
    }
    @objc private func handleFiltersButton() {
        setViewHidden(hidden: !self.filterHidden)
        filterHidden = !filterHidden
    }
    
    private func setViewHidden(hidden: Bool) {
        let filter = self.filtersView
        let result = self.resultCollection

        let downFrame = CGRect(x: 0, y: 50, width: self.view.frame.width, height: 160)
        let upFrame = CGRect(x: 0, y: -110, width: self.view.frame.width, height: 160)
        let shortFrame = CGRect(x: 0, y: 210, width: self.view.frame.width, height: self.view.frame.height - 210)
        let fillFrame = CGRect(x: 0, y: 50, width: self.view.frame.width, height: self.view.frame.height - 50)
        var resultFrame = CGRect.zero
        var filterFrame = CGRect.zero
        
//        let currentFrame = view.frame
//        var newFrame = currentFrame
        if(hidden == true){
            filterFrame = upFrame
            resultFrame = fillFrame
        } else {
            filterFrame = downFrame
            resultFrame = shortFrame
        }
        UIView.animate(withDuration: 0.4 , animations: {
            filter.frame = filterFrame
            result.frame = resultFrame
        })
    }
    
    private func processList() {
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(list.count > 30) {return 30}
        else {return list.count}
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! AnimeCell
        if(list.count > 0){
            cell.anime = list[indexPath.item]
        }
        cell.setCellShadow()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        zoomAnim(cv: collectionView, indexPath: indexPath, anime: list[indexPath.item])
    }
    
    private func zoomAnim(cv: UICollectionView, indexPath: IndexPath, anime: CellModel){
        let iv = UIImageView()
        let attrib: UICollectionViewLayoutAttributes  = cv.layoutAttributesForItem(at: indexPath)!
        attrib.frame = cv.convert(attrib.frame, to: self.view)
        attrib.frame.size = CGSize(width: attrib.frame.width, height: 0.7 * attrib.frame.height)
        iv.frame = attrib.frame
        iv.downloadedFrom(link: anime.image_url)
        iv.setCellShadow()
        let screen = UIScreen.main.bounds
        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: screen.width, height: screen.height))
        overlay.backgroundColor = UIColor.black
        overlay.alpha = 0
        self.view.addSubview(overlay)
        self.view.addSubview(iv)
        let detailsView = DetailsViewController()
        detailsView.animeCell = anime
        detailsView.mainView = self
        let toframe = CGRect(x: 15, y: 25, width: 150, height: 220)
        
        UIView.animate(withDuration: 1, animations: {
            iv.frame = toframe
            overlay.alpha = 1
        }, completion: { _ in
            self.view.willRemoveSubview(iv)
            iv.removeFromSuperview()
            self.view.willRemoveSubview(overlay)
            overlay.removeFromSuperview()
            self.navigationController?.pushViewController(detailsView, animated: false)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 115, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
}
