//
//  Models.swift
//  Anime Risuto
//
//  Created by MA on 4/22/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

struct CellModel: Decodable {
    var mal_id: Int
    var image_url: String
    var title: String
    
    init(id: Int, image:String, title:String) {
        mal_id = id
        image_url = image
        self.title = title
    }
}

struct TopListModel: Decodable {
    let list: [CellModel]
    private enum CodingKeys: String, CodingKey {
        case list = "top"
    }
}

struct SeasonListModel: Decodable {
    let list: [CellModel]
    private enum CodingKeys: String, CodingKey {
        case list = "season"
    }
}

struct SearchListModel: Decodable {
    let list: [CellModel]
    private enum CodingKeys: String, CodingKey {
        case list = "result"
    }
}


struct Genre: Decodable {
    let name: String
}

func GenreToString(_ genrelist: [Genre]) -> [String] {
    var stringList: [String] = []
    for item in genrelist {
        stringList.append(item.name)
    }
    return stringList
}

struct Company: Decodable {
    let name: String
}

struct RelatedItem: Decodable {
    let title: String
    let mal_id: Int
    let type: String
}

struct DetailsModel: Decodable {
    let mal_id: Int
    let title: String
    //let title_english: String
    let type: String
    let image_url: String
    let aired_string: String
    let source: String
    let episodes: Int
    let status: String
    let score: Double
    let rating: String
    let popularity: Int
    let favorites: Int
    let synopsis: String
    let genre: [Genre]
    let related: [String : [RelatedItem]]
    let producer: [Company]?
    let licensor: [Company]?
    let studio: [Company]?
}

struct Video: Decodable {
    let title: String
    let video_url: String
    let image_url: String
}
struct DetailsVideos: Decodable {
    let promo: [Video]
}

struct DetailsImages: Decodable {
    let image: [String]
}


