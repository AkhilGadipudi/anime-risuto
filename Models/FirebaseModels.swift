//
//  FirebaseModels.swift
//  Anime Risuto
//
//  Created by MA on 5/2/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class User: NSObject {
    var id: String?
    var name: String?
    var contry: String?
    var dob: String?
    var profileurl: String?
    
    init(dictionary: [String: AnyObject]) {
        self.id = dictionary["id"] as? String
        self.name = dictionary["name"] as? String
        self.contry = dictionary["contry"] as? String
        self.dob = dictionary["dob"] as? String
        self.profileurl = dictionary["profileurl"] as? String
    }
}

class RecentItem: NSObject {
    var mal_id: String?
    var title: String?
    var image_url: String?
    var score: String?
    var status: String?
    var rank: String
    
    init(_ dic: [String : String]){
        mal_id = dic["mal_id"]
        title = dic["title"]
        image_url = dic["image_url"]
        rank = dic["rank"] ?? "0"
        score = dic["score"]
        status = dic["status"]
    }
}
