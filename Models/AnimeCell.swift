//
//  AnimeCell.swift
//  Anime Risuto
//
//  Created by MA on 4/21/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class AnimeCell: UICollectionViewCell {
    var anime: CellModel? {
        didSet {
            cellPoster.downloadedFrom(link: (anime?.image_url)!)
            cellTitle.text = anime?.title
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let cellPoster: UIImageView = {
        let iv = UIImageView()
        //iv.layer.cornerRadius = 8
        iv.layer.masksToBounds = true
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .clear
        iv.image = #imageLiteral(resourceName: "image_placeholder")
        return iv
    }()
    
    let cellTitle: UILabel = {
        let label = UILabel()
        label.text = "Anime Title Goes Here"
        label.numberOfLines = 2
        label.textColor = UIColor.white
        label.layer.cornerRadius = 8
        label.layer.masksToBounds = true
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    private func setupViews() {
        backgroundColor = MyColors.lightBackground
        //layer.cornerRadius = 8
        addSubview(cellPoster)
        addSubview(cellTitle)
        cellPoster.frame = CGRect(x:0, y:0, width: frame.width, height:  0.70 * frame.height)
        cellPoster.setCellShadow()
        cellTitle.frame = CGRect(x:0, y: 0.7 * frame.height, width: frame.width, height: 0.3 * frame.height)
    }
}
