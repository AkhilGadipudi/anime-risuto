//
//  Messages.swift
//  Anime Risuto
//
//  Created by MA on 5/5/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class Messages {
    static public func Error(_ target: UIViewController,title: String, msg: String){
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel) { (_) in
            target.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(cancel)
        target.present(alertController, animated: true, completion: nil)
    }
}
