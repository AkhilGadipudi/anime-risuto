//
//  FirebaseHelper.swift
//  Anime Risuto
//
//  Created by MA on 5/2/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit
import Firebase

var FBhelper: FBHelper?

class FBHelper {
    var user: User?
    var userDb: DatabaseReference?
    var Lists: ListsContainer
    struct ListsContainer {
        var w2w: [String] = []
        var watching: [String] = []
        var completed: [String] = []
        var dropped: [String] = []
    }
    
    init(_ user: User) {
        self.user = user
        let ref = Database.database().reference()
        self.userDb = ref.child("users").child(user.id!)
        self.Lists = ListsContainer()
        self.getLists()
    }
    
    
    func addToRecents(values: [String: String]) {
        let userDefaults = UserDefaults.standard
        let recentsCount = userDefaults.integer(forKey: defaultKeys.recentsCountKey)
        let id = values["mal_id"] ?? "nil"
        var newVal = values
        newVal.updateValue(String(recentsCount+1), forKey: "rank")
        self.userDb?.child("recents").child(id).updateChildValues(newVal)
        userDefaults.set(recentsCount+1, forKey: defaultKeys.recentsCountKey)
    }
    
    func removeFromRecents(mal_id:String){
        self.userDb?.child("recents").child(mal_id).removeValue()
    }
    
    func addToList(to: String, id: Int){
        let lists = self.userDb?.child("lists")
        let idString = String(id)
        lists?.child(to).child(idString).setValue("nil")
        print("added to list")
        switch to {
        case ListKeys.w2w :
            Lists.w2w.append(idString)
        case ListKeys.watching :
            Lists.watching.append(idString)
        case ListKeys.completed :
            Lists.completed.append(idString)
        case ListKeys.dropped :
            Lists.dropped.append(idString)
        default:
            print("cant update local list")
        }
    }
    func removeFromList(from: String, id:Int) {
        let lists = self.userDb?.child("lists")
        let idString = String(id)
        lists?.child(from).child(idString).removeValue()
        
        switch from {
        case ListKeys.w2w :
            Lists.w2w.remove(at: Lists.w2w.index(of: idString)!)
        case ListKeys.watching :
            Lists.watching.remove(at: Lists.watching.index(of: idString)!)
        case ListKeys.completed :
            Lists.completed.remove(at: Lists.completed.index(of: idString)!)
        case ListKeys.dropped :
            Lists.dropped.remove(at: Lists.dropped.index(of: idString)!)
        default:
            print("cant update local list")
        }
    }
    func updateList(from: String, to: String, id: Int){
        if(from == to) {return}
        if(from != "nil") {
            self.removeFromList(from: from, id: id)
        }
        if(to != "nil"){
            self.addToList(to: to, id: id)
        }        
    }
    
    private func getLists() {
        let ref = self.userDb?.child("lists")
        
        // Get w2w
        ref?.child(ListKeys.w2w).observeSingleEvent(of: .value, with: {(snapshot) in
            if snapshot.childrenCount > 0 {
                self.Lists.w2w.removeAll()
                for listItem in snapshot.children.allObjects as! [DataSnapshot] {
                    self.Lists.w2w.append(listItem.key)
                }
            }
        })
        
        // Get watching
        ref?.child(ListKeys.watching).observeSingleEvent(of: .value, with: {(snapshot) in
            if snapshot.childrenCount > 0 {
                self.Lists.watching.removeAll()
                for listItem in snapshot.children.allObjects as! [DataSnapshot] {
                    self.Lists.watching.append(listItem.key)
                }
            }
        })
        
        // Get completed
        ref?.child(ListKeys.completed).observeSingleEvent(of: .value, with: {(snapshot) in
            if snapshot.childrenCount > 0 {
                self.Lists.completed.removeAll()
                for listItem in snapshot.children.allObjects as! [DataSnapshot] {
                    self.Lists.completed.append(listItem.key)
                }
            }
        })
        
        //Get Dropped
        ref?.child(ListKeys.dropped).observeSingleEvent(of: .value, with: {(snapshot) in
            if snapshot.childrenCount > 0 {
                self.Lists.dropped.removeAll()
                for listItem in snapshot.children.allObjects as! [DataSnapshot] {
                    self.Lists.dropped.append(listItem.key)
                }
            }
        })
    }
}

