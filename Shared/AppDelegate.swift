//
//  AppDelegate.swift
//  Anime Risuto
//
//  Created by MA on 4/21/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit
import Firebase

var globalWindow: UIWindow?
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
 
        let userDefaults = UserDefaults.standard
        let hasLuanched = userDefaults.bool(forKey: defaultKeys.hasLuanchedKey)
        if !hasLuanched {
            userDefaults.set(true, forKey: defaultKeys.hasLuanchedKey)
            userDefaults.set(0, forKey: defaultKeys.recentsCountKey)
        }
        let email = userDefaults.string(forKey: defaultKeys.emailKey)
        if email == nil {
            print("setting empty email and password")
            userDefaults.set("",forKey: defaultKeys.emailKey)
            userDefaults.set("", forKey: defaultKeys.passwordKey)
        }
        
        globalWindow = window
        UIApplication.shared.statusBarStyle = .lightContent
        let login = LoginRegisterViewController()
        login.topWindow = window
        let loginNav = UINavigationController(rootViewController: login)
        window?.rootViewController = loginNav
        FirebaseApp.configure()
        DropDown.startListeningToKeyboard()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

