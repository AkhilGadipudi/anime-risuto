//
//  TabBarController.swift
//  Anime Risuto
//
//  Created by MA on 5/1/18.
//  Copyright © 2018 Akhil. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
   
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let tabViewControllers = tabBarController.viewControllers!
        guard let toIndex = tabViewControllers.index(of: viewController) else {
            return false
        }
        animateToTab(toIndex: toIndex)
        return true
    }
    
    func animateToTab(toIndex: Int) {
        let tabViewControllers = viewControllers!
        guard let fromView = selectedViewController!.view else {return}
        guard let toView = tabViewControllers[toIndex].view else {return}
        guard let fromIndex = tabViewControllers.index(of: selectedViewController!) else {return}
        
        guard fromIndex != toIndex else {return}
        
        // Add the toView to the tab bar view
        fromView.superview!.addSubview(toView)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width;
        let scrollRight = toIndex > fromIndex;
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView.center = CGPoint(x: fromView.center.x + offset, y: toView.center.y)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.7, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            // Slide the views by -offset
            fromView.center = CGPoint(x: fromView.center.x - offset, y: fromView.center.y);
            toView.center   = CGPoint(x: toView.center.x - offset, y: toView.center.y);
            
        }, completion: { finished in
            
            // Remove the old view from the tabbar view.
            fromView.removeFromSuperview()
            self.selectedIndex = toIndex
            self.view.isUserInteractionEnabled = true
        })
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        let layout = UICollectionViewFlowLayout()
        let discoverView = CollectionVIewController(collectionViewLayout: layout)
        let discoverNav = UINavigationController(rootViewController: discoverView)
        discoverNav.tabBarItem.title = "Discover"
        discoverNav.tabBarItem.image = #imageLiteral(resourceName: "compass-point-7")
        
        let recentView = RecentViewController()
        let recentNav = UINavigationController(rootViewController: recentView)
        recentNav.tabBarItem.title = "Recents"
        recentNav.tabBarItem.image = #imageLiteral(resourceName: "clock-7")
        
        let libView = LibraryViewController()
        let libNav = UINavigationController(rootViewController: libView)
        libNav.tabBarItem.title = "Library"
        libNav.tabBarItem.image = #imageLiteral(resourceName: "book-cover-7")
        
        let searchView = SearchViewController()
        let searchNav = UINavigationController(rootViewController: searchView)
        searchNav.tabBarItem.title = "Search"
        searchNav.tabBarItem.image = #imageLiteral(resourceName: "search-7")
        
        self.tabBar.barTintColor = MyColors.lightBackground
        self.tabBar.tintColor = MyColors.tintBlue
        self.tabBar.unselectedItemTintColor = MyColors.textColor
        self.tabBar.isTranslucent = false
        viewControllers = [discoverNav,libNav,recentNav,searchNav]
    }
}
